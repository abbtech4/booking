package com.booking.bookingms.repository;

import com.booking.bookingms.model.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BookingRepository extends JpaRepository<Booking, UUID> {

    Optional<List<Booking>> findBookingByUserId(UUID id);
    Optional<Booking> getBookingByIdAndUserId(UUID bookingId,UUID userId);




}
