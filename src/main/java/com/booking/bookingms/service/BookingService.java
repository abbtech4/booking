package com.booking.bookingms.service;

import com.booking.bookingms.dto.BookingDTO;
import com.booking.bookingms.dto.HotelKafkaDTO;
import com.booking.bookingms.dto.KafkaDTO;
import com.booking.bookingms.model.Booking;
import com.booking.bookingms.model.BookingStatus;
import com.booking.bookingms.model.Token;
import com.booking.bookingms.repository.BookingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import java.util.List;
import java.util.UUID;
//EXCEPTION handle OLUNMALIDIR
@Service
@RequiredArgsConstructor
public class BookingService {
    private final BookingRepository bookingRepository;
    private final TokenService tokenService;
    private final KafkaTemplate<String,Object> kafkaTemplate;
    //private final HotelServiceClient hotelServiceClient;

    //Hotel ms i hazir olanda feign request yazmaq lazimdir
    @Transactional
    public String createBooking(BookingDTO bookingDTO, UUID userId,String userEmail){

        //RoomDTO roomDTO = hotelServiceClient.checkRoom(bookingDTO.getHotelId());

        //if(!roomDTO.getIsBooked()) {
            //otagin 1 gecesinin qiymeti hesablamaq uchun gunler chixilir
            long daysBetween = ChronoUnit.DAYS.between(bookingDTO.getCheckIn(), bookingDTO.getCheckOut());
            String token = UUID.randomUUID().toString();

            Booking booking = Booking.builder()
                    .adults(bookingDTO.getAdults())
                        .children(bookingDTO.getChildren())
                            .hotelId(bookingDTO.getHotelId())
                                .roomId(bookingDTO.getRoomId())
                                    .userId(userId)
                                        .finalPrice(BigDecimal.valueOf(12))//fix it per night
                                            .checkIn(bookingDTO.getCheckIn())
                                                .checkOut(bookingDTO.getCheckOut())
                                                    .bookingStatus(BookingStatus.PENDING)
                                                        .build();
            KafkaDTO kafkaDTO = KafkaDTO.builder()
                .userEmail(userEmail)
                .confirmationCode(token)
                .build();
            Token tokenConfirm = new Token(token, LocalDateTime.now(),LocalDateTime.now().plusMinutes(15),booking);
            kafkaTemplate.send("Notification",kafkaDTO);
            booking.setTokens(List.of(tokenConfirm));
            bookingRepository.save(booking);
            return token;
        //} else throw new RuntimeException("Room is booked");
    }

    //auth olunmush istifadechinin booking tarixchesi gosterilir
    public List<BookingDTO> getAuthUserAllBookings(UUID userId){
       List<Booking> bookings = bookingRepository
               .findBookingByUserId(userId).orElseThrow(()->new RuntimeException("You have no any bookings"));

      return bookings.stream().map(booking -> BookingDTO.builder()
               .adults(booking.getAdults())
               .children(booking.getChildren())
               .roomId(booking.getRoomId())
               .hotelId(booking.getHotelId())
               .checkIn(booking.getCheckIn())
               .checkOut(booking.getCheckOut()).build()).toList();
    }


    //Yazilmalidir
    public BookingDTO changeBooking(UUID userId,BookingDTO bookingDTO,UUID bookingId){
        /*feign request atilib otagin isBooked=false ve checkout tarixi bookingin checkin tarixindən
         əvvəl vəya eyni gündə olmağı if statemnetdə yoxlanılmaılıdır */
        Booking booking =  bookingRepository.getBookingByIdAndUserId(userId,bookingId).orElseThrow(()->new RuntimeException("No such booking"));
        return bookingDTO;
    }

    //bookingi legv etmek (databaseden silinsin yoxsa status rejected olsun?)
    //edit: silinsin
    @Transactional
    public BookingDTO cancelBooking(UUID bookingId,UUID userId){
       Booking booking = bookingRepository
               .getBookingByIdAndUserId(bookingId,userId).orElseThrow(()->new RuntimeException("No such booking with associated user"));
       bookingRepository.delete(booking);
       HotelKafkaDTO hotelKafkaDTO = HotelKafkaDTO.builder()
               .isBooked(false)
               .checkOut(booking.getCheckOut())
               .hotelId(booking.getHotelId())
               .roomId(booking.getRoomId()).build();
       kafkaTemplate.send("Hotel",hotelKafkaDTO);
       return BookingDTO.builder()
               .roomId(booking.getRoomId())
               .hotelId(booking.getHotelId())
               .adults(booking.getAdults())
               .children(booking.getChildren())
               .checkIn(booking.getCheckIn())
               .checkOut(booking.getCheckOut())
               .build();
    }



    @Transactional
    public String confirmBooking(String token){
        Token token1 = tokenService.getToken(token).orElseThrow(()->new RuntimeException("no such token"));

        if (token1.getConfirmedAt()!=null){
            throw new RuntimeException("booking already confirmed");
        }

        if (token1.getExpiresAt().isBefore(LocalDateTime.now())){
            throw new RuntimeException("token expired");
        }

        tokenService.confirmedAt(token);

        Booking booking = tokenService.getBookingByToken(token);
        booking.setBookingStatus(BookingStatus.COMPLETED);
        bookingRepository.save(booking);
        kafkaTemplate.send("Hotel",booking.getCheckOut());
        return "success";
    };



}
