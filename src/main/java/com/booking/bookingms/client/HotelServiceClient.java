package com.booking.bookingms.client;


import com.booking.bookingms.dto.RoomDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

@FeignClient(name = "hotel service client",path = "/hotel",url = "http://localhost:8081")
public interface HotelServiceClient {
    @GetMapping("/get/{id}")//find room by hotelId
    RoomDTO checkRoom(@RequestParam UUID hotelId);
}
