package com.booking.bookingms.task;

import com.booking.bookingms.model.Token;
import com.booking.bookingms.repository.BookingRepository;
import com.booking.bookingms.service.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ScheduledTasks {
    private final TokenService tokenService;




    @Scheduled(fixedRate = 60000*15)
    public void deleteUnverifiedTokens(){
        LocalDateTime currentTime = LocalDateTime.now();

        List<Token> findUnverifiedTokens = tokenService.findExpiredTokens(currentTime).get();

        findUnverifiedTokens.stream().filter(token -> token.getConfirmedAt()==null).forEach(tokenService::delete);
    }
}
