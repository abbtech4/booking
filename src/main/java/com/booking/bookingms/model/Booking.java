package com.booking.bookingms.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "bookings")
public class Booking {
    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;
    @Column(name = "ADULTS")
    private Integer adults;
    @Column(name = "CHILDREN")
    private Integer children;
    @Column(name = "FINAL_PRICE")
    private BigDecimal finalPrice;
    @Column(name = "CHECKIN")
    private LocalDate checkIn;
    @Column(name = "CHECKOUT")
    private LocalDate checkOut;
    @Column(name = "ROOM_ID")
    private Long roomId;
    @Column(name = "HOTEL_ID")
    private UUID hotelId;
    @Column(name = "USER_ID")
    private UUID userId;
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private BookingStatus bookingStatus;
    @OneToMany(mappedBy = "booking",cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Token> tokens;
}
