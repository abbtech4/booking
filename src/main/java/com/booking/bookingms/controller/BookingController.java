package com.booking.bookingms.controller;

import com.booking.bookingms.dto.BookingDTO;
import com.booking.bookingms.service.BookingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/booking")
@RequiredArgsConstructor
public class BookingController {

    private final BookingService bookingService;

    @PostMapping("/create")
    public ResponseEntity<String> createBooking(@RequestBody BookingDTO bookingDTO,
                                                @RequestHeader(name = "x-user-email") String userEmail,
                                                @RequestHeader(name = "x-user-id")UUID userId){
        return new ResponseEntity<>(bookingService.createBooking(bookingDTO,userId,userEmail), HttpStatus.CREATED);
    }

    @GetMapping("/get")
    public ResponseEntity<List<BookingDTO>> getAuthUserBooking(@RequestHeader(name = "x-user-id")UUID userId){
        return new ResponseEntity<>(bookingService.getAuthUserAllBookings(userId),HttpStatus.OK);
    }


    //yazılmalıdır
    @PutMapping("/update")
    public ResponseEntity<BookingDTO> updateBooking(@RequestHeader(name = "x-user-id")UUID userId,
                                                    @RequestBody BookingDTO bookingDTO){
        return new ResponseEntity<>(bookingDTO,HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<BookingDTO> cancelBooking(@PathVariable UUID id,@RequestHeader(name = "x-user-id") UUID userId){
        return new ResponseEntity<>(bookingService.cancelBooking(id,userId),HttpStatus.OK);
    }

    @GetMapping("/confirm")
    public ResponseEntity<String> confirmBooking(@RequestParam String token){
        return new ResponseEntity<>(bookingService.confirmBooking(token),HttpStatus.OK);
    }
}
