package com.booking.bookingms.dto;

import jakarta.persistence.Column;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.UUID;

@Builder
@Getter
@Setter
public class BookingDTO {
    private Integer adults;
    private Integer children;
    private LocalDate checkIn;
    private LocalDate checkOut;
    private Long roomId;
    private UUID hotelId;
}
