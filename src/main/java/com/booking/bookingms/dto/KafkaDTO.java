package com.booking.bookingms.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Data
@Builder
public class KafkaDTO {
    private String userEmail;
    private String confirmationCode;
}
